from socket import socket, AF_INET, SOCK_STREAM
from threading import Thread
from muuttujat import *


def on_ascii(v):
    # Testaa koostuuko merkkijono ASCII merkeistä
    return all(ord(c) < 128 for c in v)


def liity_palvelimeen(y):
    try:
        y.connect((OSOITE, PORTTI))
        print(' -> Yhteys palvelimeen mudostettu!')
    except ConnectionRefusedError:
        print(' -> Yhteyttä ei voitu muodostaa,')
        print(' -> Tarkista onko palvelin käynnissä.')
        exit(1)


def valita_viesteja(v):
    if v and on_ascii(v):
        try:
            yhteys.sendall(v.encode())
        except BrokenPipeError:
            print(' -> Yhteys palvelimeen on katkaistu,')
            print(' -> Kokeile uudelleen yhdistämistä.')
            exit(2)


def ota_vastaan_viesteja(y):
    while True:
        try:
            vastaanotettu_viesti = y.recv(PUSKURI).decode()
            if vastaanotettu_viesti:
                print(f'\n{vastaanotettu_viesti}')
        except OSError:
            # Main-thread sulkeutui, jos tänne päästiin.
            pass


with socket(AF_INET, SOCK_STREAM) as yhteys:
    liity_palvelimeen(yhteys)
    # Käynistetään sai, jotta saadaan viestit palvelimelta:
    Thread(target=ota_vastaan_viesteja, args=(yhteys,)).start()
    viesti = ""

    while viesti not in KOMENNOT['lopeta']:
        viesti = input('Viesti: ')
        valita_viesteja(viesti)

    print("Yhteys palvelimeen katkaistu.")


