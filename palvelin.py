from socket import socket, AF_INET, SOCK_STREAM
import threading
from muuttujat import *

# Tallennetaan kaikki connection-oliot tänne,
# jotta voidaan lähettää viestejä kaikille asiakkaille
asiakas_kanavat = []

# Lukko saikeiden koordinoitiin.
# Avataan kun edellinen yhteys on luotu onnistuneesti
lukko = threading.Lock()


class PalvelinThread(threading.Thread):

    def __init__(self, yhteys):
        super(PalvelinThread, self).__init__()
        self.yhteys = yhteys
        self.ip = None

    def odota_asiakasta(self):
        kanava, self.ip = self.yhteys.accept()
        with kanava:
            # Vapautetaan lukko, kun asiakas on yhdistetty:
            lukko.release()
            print('Uusi asiakas yhdisti palvelimeen: ', self.ip)
            asiakas_kanavat.append(kanava)
            self.kuuntele_viesteja(kanava)

    def kuuntele_viesteja(self, kanava):
        while True:
            merkkijono = kanava.recv(PUSKURI)
            if not merkkijono:
                print('Yhteys katkaistu asiakkaaseen: ', self.ip)
                break
            for k in asiakas_kanavat:
                ip, p = self.ip
                tieto = f"{ip}:{p} > {merkkijono.decode()}"
                k.sendall(tieto.encode())

    def run(self):
        # Suljetaan lukko:
        lukko.acquire()
        self.odota_asiakasta()


def kuuntele_porttia(y):
    try:
        y.bind((OSOITE, PORTTI))
        y.listen()
    except OSError:
        print(f' -> Ei voitu ottaa käyttöön porttia: {PORTTI}')
        print(' -> Varmista ettei se ole toisen ohjelman käytössä.')
        exit(1)


def main():
    with socket(AF_INET, SOCK_STREAM) as yhteys:
        # Avataan portti ja luodaan socket-palvelin:
        kuuntele_porttia(yhteys)
        while True:
            # Luodaan olio, joka alkaa kuuntelemaan asiakkaita:
            asiakas_yhteys = PalvelinThread(yhteys)
            asiakas_yhteys.start()
            # Seuraavaksi odotetaan edellisen yhteyden valmistumista:
            with lukko:
                continue


main()



